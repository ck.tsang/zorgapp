package adsd.app.zorgapp;

public class Medicijn {

	private String medicijnNaam;
	private String omschrijving;
	private String soort;
	private String dosering;
	
	public Medicijn(){
		this(null, null, null, null);
	}
	
	public Medicijn(String medicijnNaam, String omschrijving, String soort, String dosering) {
		this.medicijnNaam = medicijnNaam;
		this.omschrijving = omschrijving;
		this.soort=soort;
		this.dosering = dosering;
	}
	
	public void setMedicijnNaam(String naam){
		medicijnNaam = naam;
	}
	
	public String getMedicijnNaam(){
		return medicijnNaam;
	}
	
	public void setOmschrijving(String omschrijving){
		this.omschrijving = omschrijving;
	}
	
	public String getOmschrijving(){
		return omschrijving;
	}
	
	public void setSoort(String soort){
		this.soort = soort;
	}
	
	public String getSoort(){
		return soort;
	}
	
	public void setDosering(String dosering){
		this.dosering = dosering;
	}
	
	public String getDosering(){
		return dosering;
	}
	
	public String toString() {
		String med = String.format("%s\t%s\t%s\t%s\n", medicijnNaam, soort, dosering, omschrijving);
		return med;
	}
}
