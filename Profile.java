package adsd.app.zorgapp;
import java.util.Locale;

public class Profile {
	private String voorNaam;
	private String achterNaam;
	private int leeftijd;
	private double gewicht;
	private double lengte;
	
	public Profile() {
		this(null, null, 0, 0.0, 0.0);
	}
	
	public Profile(String voorNaam, String achterNaam, int leeftijd) {
		this(voorNaam, achterNaam, leeftijd, 0.0, 0.0);
	};
	
	public Profile(String voorNaam, String achterNaam, int leeftijd, double gewicht, double lengte) {
		this.voorNaam = voorNaam;
		this.achterNaam = achterNaam;
		this.leeftijd = leeftijd;
		this.gewicht = gewicht;
		this.lengte = lengte;
	}
	
	public void setVoorNaam(String voorNaam) {
		this.voorNaam = voorNaam;
	}
	
	public String getVoorNaam() {
		return voorNaam;
	}
	
	public void setAchterNaam(String achterNaam) {
		this.achterNaam = achterNaam;
	}
	
	public String getAchterNaam() {
		return achterNaam;
	}
	
	public void setGewicht(double gewicht) {
		this.gewicht = gewicht;
	}
	
	public double getGewicht() {
		return gewicht;
	}
	
	public void setLengte(double lengte) {
		this.lengte = lengte;
	}
	
	public double getLengte() {
		return lengte;
	}
	
	public void setLeeftijd(int leeftijd) {
		this.leeftijd = leeftijd;
	}
	
	public int getLeeftijd() {
		return leeftijd;
	}
	
	public String getBmi() {
		double bmi = gewicht / (lengte * lengte);
		return String.format(Locale.US,"%.1f", bmi);
	}
}
