package adsd.app.zorgapp;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;
import java.util.Properties;
import java.util.ResourceBundle;

public class ZorgApp {

	private Profile profile;
	private Medicijn medicijn;
	private GewichtMeetPunt meetpunt;
	private ArrayList<GewichtMeetPunt> gewichtMeetPuntData;
	private ArrayList<Medicijn> medicijnLijst;
	
	public void ZorgApp() {
		profile = new Profile("Jan", "Jansen", 30, 65, 1.80); //maak een profiel aan
		medicijnLijst = new ArrayList<Medicijn>(); //maak medicatielijst aan
		gewichtMeetPuntData = new ArrayList<GewichtMeetPunt>();//maak gewichtpuntlijst aan
		
		//aanmaken van medicijnen
		medicijn = new Medicijn("Paracetamol", "Pijnstillend en koortsverlagend", "Pijnstiller", "Max 3x daags 1000mg");
		medicijnLijst.add(medicijn);
		
		medicijn = new Medicijn("Ibuprofen", "Pijnstillend en koortsverlagend", "Pijnstiller", "Max 3x daags 400mg");
		medicijnLijst.add(medicijn);
		
		medicijn = new Medicijn("Omeprazol", "Tegen brandend maagzuur", "Maagzuurremmer", "Max 1x daags 20mg");
		medicijnLijst.add(medicijn);
		
		//aanmaken van gewichtmeetpunten
		meetpunt = new GewichtMeetPunt("15-08-2020", "12:00", 50);
		gewichtMeetPuntData.add(meetpunt);
		
		meetpunt = new GewichtMeetPunt("05-10-2020", "13:00", 58);
		gewichtMeetPuntData.add(meetpunt);
		
		meetpunt = new GewichtMeetPunt("01-12-2020", "11:00", 67);
		gewichtMeetPuntData.add(meetpunt);
		
		meetpunt = new GewichtMeetPunt("04-02-2021", "12:30", 85);
		gewichtMeetPuntData.add(meetpunt);
		
		meetpunt = new GewichtMeetPunt("15-04-2021", "12:00", 75);
		gewichtMeetPuntData.add(meetpunt);
		
	}

	public void viewData() {
		boolean stoppenProgramma=false;
		while(!stoppenProgramma) {
			int hoofdMenuLoop = 0;
			Scanner sc = new Scanner(System.in); //om gebruik te maken van het toetsenbord
			ResourceBundle rb = ResourceBundle.getBundle("adsd/app/cfg/resource_bundle");
			System.out.println(rb.getString("language"));
			int taalKeuze = sc.nextInt();
			switch(taalKeuze) {
				case 1:
					Locale.setDefault(new Locale("nl", "NL"));
					break;
				
				case 2:
					Locale.setDefault(new Locale("en", "US"));
					break;
					
				case 3:
					Locale.setDefault(new Locale("de", "DE"));
					break;
				
				case 4:
					stoppenProgramma=true;
					hoofdMenuLoop=3;
					System.out.println(rb.getString("stop"));
					sc.close();
					break;
				
				default:
					hoofdMenuLoop=3;
					System.out.println(rb.getString("wronginput"));
			}
			while(hoofdMenuLoop!=3){
				rb = ResourceBundle.getBundle("adsd/app/cfg/resource_bundle");
				System.out.println(rb.getString("role")); //laat het hoofdmenu zien met 3 opties
				
				hoofdMenuLoop = sc.nextInt();
				if(hoofdMenuLoop==1){ //menu voor zorgverlener
					boolean stoppenVerlener = false; //wordt later gebruikt ter controle om de loop te stoppen
					while(!stoppenVerlener) {
						System.out.println("\n" + rb.getString("mainCarer"));
						
						switch(sc.nextInt()) {
						case 1: //toon profielgegevens op basis van beschikbaarheid
							sc.nextLine();
							getProfiel();
							break;
							
						case 2: //bewerk profielgegevens
							int profileLoop = 0;
							sc.nextLine();
							while(profileLoop<6) {
								System.out.println(rb.getString("editProfile"));
								profileLoop = sc.nextInt();
								sc.nextLine();
								if(profileLoop==1) {
									System.out.println(rb.getString("editFirstName") + ":");
									profile.setVoorNaam(sc.nextLine());
								}
								if(profileLoop==2) {
								System.out.println(rb.getString("editLastName") + ":");
								profile.setAchterNaam(sc.nextLine());
								}
								if(profileLoop==3) {
								System.out.println(rb.getString("editAge") + ":");
								profile.setLeeftijd(sc.nextInt());
								}
								if(profileLoop==4) {
								System.out.println(rb.getString("editWeight") + "(kg):");
								profile.setGewicht(sc.nextDouble());
								}
								if(profileLoop==5) {
								System.out.println(rb.getString("editLength") + "(m):");
								double nLengte = Double.parseDouble(sc.nextLine());
								profile.setLengte(nLengte);
								}
								else if(profileLoop>=6) {
									System.out.println(rb.getString("back"));
								}
							}
							break;
	
						case 3: //toon medicatielijst
							getMedicijn();
							break;
							
						case 4: //bewerk medicatielijst
							System.out.println(rb.getString("choose") + ":");
							getMedicijn();
							int medKeuze = sc.nextInt() -1;
							int medLoop = 0;
							sc.nextLine();
							System.out.println(medicijnLijst.get(medKeuze));
							while(medLoop < 5) {
								System.out.println(rb.getString("editMeds"));
								medLoop = sc.nextInt();
								sc.nextLine();
								if(medLoop==1) {
									System.out.println(rb.getString("editName"));
									medicijnLijst.get(medKeuze).setMedicijnNaam(sc.nextLine());
								}
								if(medLoop==2) {
									System.out.println(rb.getString("editType"));
									medicijnLijst.get(medKeuze).setSoort(sc.nextLine());
								}
								if(medLoop==3) {
									System.out.println(rb.getString("editDose"));
									medicijnLijst.get(medKeuze).setDosering(sc.nextLine());
								}
								if(medLoop==4) {
									System.out.println(rb.getString("editDescription"));
									medicijnLijst.get(medKeuze).setOmschrijving(sc.nextLine());
								}
								else if(medLoop>=5) {
									System.out.println(rb.getString("back"));
								}
							} 
							break;
							
						case 5: //toon gewichtgrafiek
							getGewichtGrafiek();
							break;
						case 6:
							stoppenVerlener = true; //stopt de loop
							break;
						
						default:
							System.out.println(rb.getString("wronginput") + "\n");
						}
					}
				}
				
				else if(hoofdMenuLoop==2) { //menu voor pati�nt
					boolean stoppenPatient = false;
					while(!stoppenPatient) {
						System.out.println("\n" + rb.getString("mainPatient"));
						switch(sc.nextInt()) {
						case 1:
							sc.nextLine();
							getProfiel();
							break;
							
						case 2:
							int profilePatientLoop = 0;
							sc.nextLine();
							while(profilePatientLoop<4) {
								System.out.println(rb.getString("editPatientProfile"));
								profilePatientLoop = sc.nextInt();
								sc.nextLine();
								if(profilePatientLoop==1) {
									System.out.println(rb.getString("editFirstName") + ":");
									profile.setVoorNaam(sc.nextLine());
								}
								if(profilePatientLoop==2) {
									System.out.println(rb.getString("editLastName") + ":");
									profile.setAchterNaam(sc.nextLine());
								}
								if(profilePatientLoop==3) {
									System.out.println(rb.getString("editAge") + ":");
									profile.setLeeftijd(sc.nextInt());
								}
								else if(profilePatientLoop>=4) {
									System.out.println(rb.getString("back"));
								}
							} 
							break;
							
						case 3: 
							getMedicijn();
							break;
						
						case 4:
							getGewichtGrafiek();
							break;
							
						case 5:
							stoppenPatient = true; //stopt de loop
							break;
						
						default:
							System.out.println(rb.getString("wronginput") + "\n");
						}
					}
				}
				else if(hoofdMenuLoop==3) {
					System.out.println(rb.getString("back"));
				}
				else {
					System.out.println(rb.getString("wronginput") + "\n");
				}
			}
		}
	}
	private void getProfiel() {
		ResourceBundle rb = ResourceBundle.getBundle("adsd/app/cfg/resource_bundle");
		System.out.println(rb.getString("name") + ": " + profile.getVoorNaam() + " " + profile.getAchterNaam());
		System.out.println(rb.getString("age") + ": "+ profile.getLeeftijd() + " " + rb.getString("year"));
		System.out.println(rb.getString("weight") + ": " + profile.getGewicht() + "kg");
		System.out.println(rb.getString("length") + ": " + String.format("%.2f", profile.getLengte()) + "m");
		double bmi = Double.parseDouble(profile.getBmi());
		if(bmi <= 18.5) {
			System.out.println("BMI: " + profile.getBmi() + " [" + rb.getString("underweight") +"]");
			}
		else if(bmi > 18.5 && bmi <25) {
			System.out.println("BMI: " + profile.getBmi() + " [" + rb.getString("normal") + "]");
			}
		else if(bmi > 25 && bmi <30) {
			System.out.println("BMI: " + profile.getBmi() + " [" + rb.getString("overweight") +"]");
			}
		else if(bmi >= 30) {
			System.out.println("BMI: " + profile.getBmi() + " [" + rb.getString("obese") +"]");
			}
	}
	
	private void getMedicijn() {
		ResourceBundle rb = ResourceBundle.getBundle("adsd/app/cfg/resource_bundle");
		String medString = "";
		int medInt = 1;
		for(Medicijn m : medicijnLijst) {
			medString = medString + medInt + ". " + m.toString();
			medInt++;
		}
		System.out.println(rb.getString("meds"));
		System.out.println(medString);
	}
	
	private void getGewichtGrafiek() {
		ResourceBundle rb = ResourceBundle.getBundle("adsd/app/cfg/resource_bundle");
		System.out.print(rb.getString("date") + "\t\t" + rb.getString("time") + "\t" + rb.getString("weight") + "\t-----------25kg--------50kg---------75kg---------100kg  -->" + rb.getString("weight") + "(kg)\to = 2kg");
		for(GewichtMeetPunt g : gewichtMeetPuntData) {
			System.out.print(String.format("\n%s\t%s\t%.0fkg\t", g.getDatum(),g.getTijd(), g.getGewicht()));
			double gewichtIndicator = g.getGewicht() /2;
				for(int i = 0; i < gewichtIndicator; i++) {
					System.out.print("o");
				}
			}
		System.out.println("\n");
		}
}